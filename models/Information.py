import uuid
from datetime import datetime
from cassandra.cqlengine import columns
from models.Base import Base

class Information(Base):
    id = columns.UUID(primary_key=True, default=uuid.uuid4)
    created_at = columns.Date(default=datetime.now())
    url = columns.Text()
    domain = columns.Text()
    cache_tag = columns.Text()

    def get_data(self):
        return {
            'id': str(self.id),
            'created_at': self.created_at,
            'url': self.url,
            'domain': self.domain,
            'cache_tag': self.cache_tag
        }
