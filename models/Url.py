import uuid
from datetime import datetime
from cassandra.cqlengine import columns
from models.Base import Base


class Url(Base):
    id = columns.Integer(primary_key=True)
    created_at = columns.Time()
    url = columns.Text()
    domain = columns.Text()
    cache_tag = columns.Text()

    def get_data(self):
        return {
            'id': str(self.id),
            'created_at': self.created_at,
            'url': self.url,
            'domain': self.domain,
            'cache_tag': self.cache_tag
        }
