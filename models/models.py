import os
import pymongo
from cassandra.cqlengine import management
from cassandra.cluster import Cluster
from models.Information import Information

cluster = Cluster(['10.5.21.93'])

db = cluster.connect('cache_metatdata')
# myclient = pymongo.MongoClient(os.getenv("MONGO_DATABASE"))
# db = myclient["data-test"]

keyspaces = ['cdn', 'cdn', 'cdn']
conns = ['cluster1', 'cluster2', 'cluster3']

# registers your connections
# ...

# create all keyspaces on all connections
# for ks in keyspaces:
#     management.create_simple_keyspace(ks, connections=conns)

# define your Automobile model
# ...

# sync your models
management.sync_table(Information, keyspaces=keyspaces, connections=conns)
