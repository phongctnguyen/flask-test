from flask import Blueprint
from flask_restful import Api
from api.resources.Hello import Hello, Cars, Users, NewUser
from api.resources.resource import Urls

blueprint = Blueprint('api', __name__)
api = Api(blueprint)

api.add_resource(Hello, '/')
api.add_resource(Cars, '/cars')
api.add_resource(NewUser, '/users')
api.add_resource(Users, '/users/<string:id>')

api.add_resource(Urls, '/url')
# api.add_resource(Users, '/users')
