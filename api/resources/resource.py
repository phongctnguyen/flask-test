from flask import request
from flask_restful import Resource
from uuid import uuid4
from models.Url import Url
import json
from datetime import datetime


class Urls(Resource):
    def get(self):
        time1 = datetime.now()
        infos = Information.objects().all()
        # infos1 = Information.objects.using(connection='cluster1').all()
        # infos2 = Information.objects.using(connection='cluster2').all()
        # infos3 = Information.objects.using(connection='cluster3').all()

        new_arr = [info.get_data()['domain'] for info in infos]
        # new_arr.append([info.get_data() for info in infos1])
        # new_arr.append([info.get_data() for info in infos2])
        # new_arr.append([info.get_data() for info in infos3])

        # for info in infos1:
        #   if info.get_data()['domain'] not in new_arr:
        #     new_arr.append(info.get_data()['domain'])
        
        # for info in infos2:
        #   if info.get_data()['domain'] not in new_arr:
        #     new_arr.append(info.get_data()['domain'])

        # for info in infos3:
        #   if info.get_data()['domain'] not in new_arr:
        #     new_arr.append(info.get_data()['domain'])

        for el in new_arr:
            print(el)
        # return {"data": new_arr}, 200
        time2 = datetime.now()
        print(time2 - time1)
        return 200

    def post(self):
        print('post')
        data = request.json
        url = Url.objects.create(**data)
        url.save()

        # info1 = Information.objects.using(
        #     connection='cluster1').create(**data)
        # info1.save()

        # info2 = Information.objects.using(
        #     connection='cluster2').create(**data)
        # info2.save()

        # info3 = Information.objects.using(
        #     connection='cluster3').create(**data)
        # info3.save()
        # try:
        #     person1 = Person.objects.using(
        #         connection='cluster2').create(**data)
        #     person1.save()
        # except:
        #     pass
        return 201
