from flask import request
from flask_restful import Resource
# from models.models import db
from uuid import uuid4
from models.Person import Person
import json


class Hello(Resource):
    def get(self):
        return 'Hello'


class Cars(Resource):
    def get(self):
        # db.cars.insert_one({'name': 'Mercedes', 'price': 57127})
        return 200


class NewUser(Resource):
    def get(self):
        # print('read')
        # rows = db.execute(
        #     'SELECT client_id, client_age, client_email, client_name FROM client')
        # for row in rows:
        #     print(row.client_id, row.client_age,
        #           row.client_email, row.client_name)
        persons = Person.objects().all()
        new_arr = [person.get_data() for person in persons]
        return {"data": new_arr}, 200
        # return 200

    def post(self):
        print('post')
        data = request.json
        # db.execute(
        #     """
        #     INSERT INTO client (client_id, client_age, client_email, client_name)
        #     VALUES (%s, %s, %s, %s)
        #     """,
        #     (data['client_id'], data['client_age'],
        #      data['client_email'], data['client_name'],)
        # )
        person = Person.objects.create(**data)
        person.save()
        try:
            person1 = Person.objects.using(connection='cluster2').create(**data)
            person1.save()
        except:
            pass
        return person.get_data()
    

class Users(Resource):
    def patch(self, id):
        data = request.json
    #   session.execute("UPDATE users SET age =%s WHERE client_id = %s", [
    #                   new_age, id])
        # for key in data:
        #     print(key)
        # person = Person()
        # new_dict = [a for a in dir(person) if not a.startswith('__')]
        # print(new_dict)

        # for el in new_dict:
        #     if el in data:
        #         Person.objects(id=id).update(**data) 
        Person.objects(id=id).update(**data)
        return 200

    def delete(self, id):
        Person.objects(id=id).delete()
        return 204

