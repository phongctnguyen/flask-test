from flask import Flask
from app import create_app
from models.Information import Information
from cassandra.cqlengine.management import sync_table
from cassandra.cqlengine import management
from cassandra.cqlengine import connection
from cassandra.cluster import Cluster

if __name__ == "__main__":
    app = create_app("config")

    # keyspaces = ['cdn', 'cdn', 'cdn']
    # conns = ['cluster1', 'cluster2', 'cluster3']
    # connection.setup(['10.5.21.137', '10.5.21.205'],
    #                  "cqlengine", protocol_version=3)
    # # try:
    #     connection.setup(['127.0.0.1'], "cqlengine", protocol_version=3)
    # except:
    #     connection.setup(['10.5.21.63'], "cqlengine", protocol_version=3)
    # # try: 
    #     connection.setup(['10.5.21.137, 10.5.21.205'],
    #                      "cqlengine", protocol_version=3)
        # connection.register_connection('cluster1', ['10.5.21.93'])
        # connection.register_connection('cluster1', ['10.5.21.205'])
        # connection.register_connection('cluster3', ['10.5.22.80'])
        # sync_table(Information)
    # except:
    #     connection.setup(['10.5.21.137'], "cqlengine", protocol_version=3)
        # connection.register_connection('cluster2', ['10.5.21.63'])
    # management.sync_table(Information, keyspaces=keyspaces, connections=conns)
    # sync_table(Information)
    app.run(debug=True)
