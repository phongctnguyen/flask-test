from flask import Flask
from api.route import blueprint

def create_app(config_filename):
    app = Flask(__name__)
    app.config.from_object(config_filename)
    app.register_blueprint(blueprint)

    return app